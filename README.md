## Synopsis

PiSentry is a stand alone camera system that runs on a Raspberry Pi with this configuration package to setup headless mode.

This configuration package does not have any interface and is only run at package installation time.

## Build

### Cross compile and packaging

To cross compile the application run the following command.

    sudo make pkg

## Installation

This app is packaged in an opkg format.  After building look in the pkg directory for an .opk file.  This is the file to be installed on the target system.

To install the package on the target, copy the file to the system or the SD card that is used to boot the system.  Then use the following command.

    opkg install <path to file>/pisentrycfg_1.0_arm.opk

If the package has been installed previously, use the following command to reinstall it.

    opkg --force-reinstall install <path to file>/pisentrycfg_1.0_arm.opk

## Contributors

To get involved with PiBox, contact the project administrator:
Michael J. Hammel <mjhammel@graphics-muse.org>

## License

0BSD

