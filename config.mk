#----------------------------------------------------------------------
# Common Config variables and targets.
# Includes configurations from configs directory.
#----------------------------------------------------------------------

#---------------------------------------------------------------------
# Edit these for a new build environment

# Project Architecture and Vendor
ARCH := arm

# Hardware: PiSentry requires rpi2
HW = rpi2

# Build Version ID
BLD_VERSION := $(shell cat version.txt)

#---------------------------------------------------------------------
# End of Build Configurable options
#---------------------------------------------------------------------
# Number of parallel jobs.  Override this on the command line.
JOBS = 4

# Who am I?
UID = $(shell id -u)

#---------------------------------------------------------------------
# Target names - these are also used with a "." prefix as
# empty target files for various build sections.
# TARGETS gets updated in each component's .cfg file.

INIT_T          := init
TARGETS         = 

#---------------------------------------------------------------------
# Directories: 
# The build and archive downloads are kept in parallel directories from
# the source tree so hg status won't get confused by all the new files.
TOPDIR              := $(shell pwd)
SRCDIR              := $(TOPDIR)/src
ARCDIR              := $(TOPDIR)/../archive
BLDDIR              := $(TOPDIR)/../bld
PKGDIR              := $(TOPDIR)/../pkg
SCRIPTDIR           := $(TOPDIR)/scripts

#---------------------------------------------------------------------
# These variables can be set as environment variables on the "make" command line

# Where we can find the opkg-build utility is set with OPKG
ifeq ($(OPKG),)
OPKG_DIR			= /usr/local/bin
else
OPKG_DIR			= $(OPKG)
endif

#---------------------------------------------------------------------
# Include the configs directory files after the common configs
# Note: Order here is important

include configs/pisentrycfg.cfg

#---------------------------------------------------------------------
# Include the component makefiles
# Note: Order here is important
include configs/pisentrycfg.mk

#---------------------------------------------------------------------
# Config display target
showconfig:
	@$(MSG3) Common Configuration $(EMSG)
	@echo "Components           :$(TARGETS)"
	@echo "Version              : $(BLD_VERSION)"
	@echo "ARCH                 : $(ARCH)"
	@echo "HW                   : $(HW)"
	@echo "SRCDIR               : $(SRCDIR)"
	@echo "ARCDIR               : $(ARCDIR)"
	@echo "BLDDIR               : $(BLDDIR)"
	@echo "OPKG_DIR             : $(OPKG_DIR)"

