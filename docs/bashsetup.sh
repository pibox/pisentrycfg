#!/bin/bash -p
# Functions and aliases used to nagivate project directories.
# Include by the ~/bin/cdtools script.
#############################################################################

# -------------------------------------------------------------------
# DESC: pisentrycfg opkg build
# -------------------------------------------------------------------
function pisentrycfg {

    # If supplied, the first argument is a suffix, to allow multiple versions of the same tree.
    SFX=$1

    # -------------------------------------------------------------------
    # Edit these items
    # -------------------------------------------------------------------

    # Top of the source tree
	# The source, build, archive and package directories will live
	# under this tree.
    SRCTOP=<SET A PATH HERE>

    # -------------------------------------------------------------------
    # Don't edit below here
    # -------------------------------------------------------------------
    # Project ID
    PRJ=pisentrycfg

	# GIT Repo
	export GITREPO=git@gitlab.com:pibox/$PRJ.git

    # Where I do my dev work
    GM_WORK=$SRCTOP/work
    # Where the CVS is located
    GM_HOME=$SRCTOP/$PRJ$SFX
    # Where the source and build directories live
    GM_SRC=$GM_HOME/src
    GM_BUILD=$GM_HOME/bld
    GM_ARCHIVE=$GM_HOME/archive
    GM_PKG=$GM_HOME/pkg
    GM_EXTRAS=$GM_HOME/extras

    # Put the host applications in our path
    # export PATH=$GM_TOOLS:$PATH

    # Make the configured environment available to the build system.
    export GM_WORK
    export GM_ARCHIVE
    export GM_PKG
    export GM_HOME
    export GM_SRC
    export GM_BUILD
    export GM_EXTRAS

    # Some aliases to bounce around directories easily
    alias cdt='cd $SRCTOP'
    alias cdh='cd $GM_HOME'
    alias cdw='cd $GM_WORK'
    alias cdx='cd $GM_SRC'
    alias cdb='cd $GM_BUILD'
    alias cda='cd $GM_ARCHIVE'
    alias cdp='cd $GM_PKG'
    alias cde='cd $GM_EXTRAS'

    # Show the aliases for this configuration
    alias cd?='cdpisentrycfg'
}
function cdpisentrycfg {
echo "
$PRJ Alias settings:
-----------------------------------------------------------------------------
cdt    cd SRCTOP ($SRCTOP)
cdh    cd GM_HOME ($GM_HOME)
cdw    cd GM_WORK ($GM_WORK)
cdx    cd GM_SRC ($GM_SRC)
cdb    cd GM_BUILD ($GM_BUILD)
cda    cd GM_ARCHIVE ($GM_ARCHIVE)
cdp    cd GM_PKG ($GM_PKG)
cde    cd GM_EXTRAS ($GM_EXTRAS)

To checkout tree:
cdt
mkdir $PRJ$SFX
cdh
git clone $GITREPO src

Pushing your local repository:
    cd <src>
    git init
    git add .
    git commit
    git checkout master
    git remote add origin $GITREPO
    git push -u origin master
"
}

