# PiSentry Configuration App.
# Creates an opkg that modifies PiBox for PiSentry.
# ----------------------------------------------------
all: defaultBB

# These files contain common variables and targets.
include config.mk
include util.mk

# ---------------------------------------------------------------
# Default build
# ---------------------------------------------------------------
defaultBB: .$(WWW_T) 

# ---------------------------------------------------------------
# Cleanup targets - seldom used since they affect all 
# components at once.
# ---------------------------------------------------------------
clean: 
	@for component in $(TARGETS); do \
		echo "Clobbering: $$component"; \
		make --no-print-directory $$component-clean; \
	done

# Careful - this wipes your archive out too!
clobber: 
	@for component in $(TARGETS); do \
		echo "Clobbering: $$component"; \
		make --no-print-directory $$component-clobber; \
	done
	@rm -f Changelog*
	@rm -rf $(ARCDIR) $(BLDDIR) $(PKGDIR)

